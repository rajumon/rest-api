<?php

namespace App\Http\Controllers;

use App\Helpers\APIHelpers;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Models\Device;
use Validator;
use Carbon\Carbon;

class DeviceController extends Controller
{
    public function list($id=null){
        $device = $id?Device::find($id):Device::all();
        $response = APIHelpers::createAPIHelpers(false, 200, '' , $device);
        return response()->json($response, 200);
    }
    public function add(Request $request){
        $rules = array(
            'name'=>'required',
            'member_id'=>'required'
        );
        $data = array("created_at" => Carbon::now(), "updated_at" => now());
        $data['name'] = $request->name;
        $data['member_id'] = $request->member_id;

        $Validator = Validator::make($data, $rules);

        if ($Validator->fails()) {
            return response()->json($Validator->errors(), 401);
        }else{
            $insertData = Device::insert($data);
            if($insertData){
                $response = APIHelpers::createAPIHelpers(false, 201, 'Data added successfully.' , null);
                return response()->json($response, 200);
            }else{
                $response = APIHelpers::createAPIHelpers(true, 201, 'Data created failed.' , null);
                return response()->json($response, 400);
            }

        }
    }

    public function update(Request $request){

        $data = array();
        $data['name'] = $request->name;
        $data['member_id'] = $request->member_id;
        $result = Device::find($request->id)->update($data);
        if ($result){
            return response(["message" => "Updated successfully"], 200);
        }
        return response(["message" => "Faild"], 401);
    }

    public function search($name){
        $result = Device::where('name',"like","%".$name."%")->get();

        if (!count($result)) {
            return response()->json($name." - not found.", 404);
        }
        return response()->json($result, 200);
    }

    public function delete($id){
        $device = Device::find($id);

        if ($device != null){
            $result = $device->delete();
            return response()->json( $id." delete successfully.", 200);
        }
        return response()->json($id." - not found.", 404);
    }

}
