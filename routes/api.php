<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\dummyApi;
use App\Http\Controllers\DeviceController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\Api\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('data', [dummyApi::class, 'getData']);

Route::get('list/{id?}', [DeviceController::class, 'list']);
Route::post('add', [DeviceController::class, 'add']);
Route::put('update', [DeviceController::class, 'update']);
Route::get('search/{name}', [DeviceController::class, 'search']);
Route::delete('delete/{id}', [DeviceController::class, 'delete']);

Route::apiResource('member', MemberController::class);
Route::post('upload', [FileController::class, 'upload']);

Route::group([
    'prefix' => 'auth'
], function () {

    Route::post('login', [AuthController::class,'login']);
    Route::post('logout', [AuthController::class,'logout']);
    Route::post('refresh', [AuthController::class,'refresh']);
    Route::post('me', [AuthController::class,'me']);
    Route::post('payload', [AuthController::class,'payload']);
    Route::post('register', [AuthController::class,'register']);

});
