<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');



Route::get('login/google', [HomeController::class,'redirectToGoogle'])->name('login.google');
Route::get('login/google/callback', [HomeController::class,'handleGoogleCallback']);

Route::get('login/facebook', [HomeController::class,'redirectToFacebook'])->name('login.facebook');
Route::get('login/facebook/callback', [HomeController::class,'handleFacebookCallback']);

Route::get('login/github', [HomeController::class,'redirectToGithub'])->name('login.github');
Route::get('login/github/callback', [HomeController::class,'handleGithubCallback']);
